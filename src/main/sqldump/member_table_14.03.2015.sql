--
-- Структура таблицы `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` enum('Regular','Editor') COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `member`
--

INSERT INTO `member` (`id`, `role`, `username`, `email`, `firstName`, `lastName`, `password`) VALUES
  (1, 'Editor', 'quizmaster', 'quizmaster@jquiz.com', 'Master', 'Quiz', '06405446f73ec9a2d6dc70f1289a18ba'),
  (2, 'Regular', 'user01', 'user01@jquiz.com', 'Test', 'User', 'b75705d7e35e7014521a46b532236ec3');