package com.jquiz.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import com.jquiz.form.LoginForm;
import com.jquiz.model.Member;
import com.jquiz.service.Auth;
import com.jquiz.service.Router;
import org.apache.commons.codec.digest.DigestUtils;

public class MemberServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {

        String action = Router.getActionName(req);
        System.out.println("Action: " + action);
        switch (action) {
            
            case "login":
                loginForm(req, resp);
                break;

            case "logout":
                logout(req, resp);
                break;

            case "register":
                registerForm(req, resp);
                break;

            case "profile":
                profilePage(req, resp);
                break;

            case "list":
                listMembers(req, resp);
                break;
            
            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = Router.getActionName(req);
        switch (action) {
            case "login":
                processLogin(req, resp);
                break;

            case "register":
                processRegister(req, resp);
                break;

            default:
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        
    }
    
    /** Actions */
    
    protected void loginForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }
        
        req.setAttribute("page_title", "Login");
        req.setAttribute("form", LoginForm.emptyForm());
        req.getRequestDispatcher("/memberLoginForm.jsp").forward(req, resp);
    }

    protected void processLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // check if member already logged in
        HttpSession session = req.getSession();
        if (Auth.isLogged(session.getId())) {
            String contextPath = req.getContextPath();
            resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
            return;
        }
        
        LoginForm form = LoginForm.fromRequest(req);
        form.validate();
        if (!form.hasErrors()) {
            Member member;
            try {
                member = Member.findMemberByUsername(form.getValue("username"));
                if (member.passwordSecured.equals(DigestUtils.md5Hex(form.getValue("password")))) {
                    Auth.createAuth(member, session.getId());
                    String contextPath = req.getContextPath();
                    resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
                    return;
                }
            } catch (Exception e) {}
            form.setError("general", "Username or Password Incorrect");
        } else {
            form.setError("general", "Error occurred");
        }

        req.setAttribute("page_title", "Login");
        req.setAttribute("form", form);
        req.getRequestDispatcher("/memberLoginForm.jsp").forward(req, resp);
    }

    protected void logout(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException  {
        HttpSession session = req.getSession();
        Auth.destroyAuth(session.getId());
        String contextPath = req.getContextPath();
        resp.sendRedirect(resp.encodeRedirectURL(contextPath + "/"));
    }

    protected void registerForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        throw new ServletException("TODO: Implement me");
    }

    protected void processRegister(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        throw new ServletException("TODO: Implement me");
    }

    protected void profilePage(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        throw new ServletException("TODO: Implement me");
    }

    protected void listMembers(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        throw new ServletException("TODO: Implement me");
    }
    
}
