package com.jquiz.service;

import javax.servlet.http.HttpServletRequest;

/**
 * Url router/mapper tools
 */
public class Router {

    /**
     * Return "controller" part from URI http://%appbase%/controller/action?param
     * 
     * @param req HttpServletRequest
     * @return String
     */
    public static String getControllerName(HttpServletRequest req) {
        
        // TODO: consider case when "/" character presents in GET-parameters 
        
        String[] parts = req.getRequestURI().split("/");
        if (parts.length > 1) {
            return parts[parts.length - 2];
        } else {
            return parts[parts.length - 1];
        }
    }

    /**
     * Return "action" part from URI http://%appbase%/controller/action?param
     *
     * @param req HttpServletRequest
     * @return String
     */
    public static String getActionName(HttpServletRequest req) {

        // TODO: consider case when "/" character presents in GET-parameters
        
        String[] parts = req.getRequestURI().split("/");
        return parts[parts.length - 1];
    }
}
